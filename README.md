Hi there! 

This is a project done while studying at Escola Massana

In this project we use Pd + Processing to create a program + interface to make people with low knowledge on phonetics/music/programming skills be able to play around with formants and almost-human-voices and nearly-alien-voices

if you have Pd installed you must add the folder "cajitas" to the search path in the startup preferences (as some sketches here uploaded make use of other sketches to work)

And if youre using the RECEIVER.pd you must click the message "port 9010" and make sure processing is sending all messages to that port (also make sure your computer is not already using that port for some stuff bc then pd might get a bit crazy)

enjoy!