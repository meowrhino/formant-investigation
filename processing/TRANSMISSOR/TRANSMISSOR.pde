import oscP5.*;
import netP5.*;
import controlP5.*;
ControlP5 cp5;
OscP5 oscP5;

// TO WORK: QUIZA QUITAR LO DE a1 a2 SSUTAIN Y a4 PORQUE DA IGUAL Y TAL? PONER SIMPLEMENTE FUERZA Y TIEMPO?
//TOWORK 2: CAMBIAR a1 POR A1, a2 POR A2 (DE AMPLITUDE) Y a1T POR T1 Y a2T POR T2
NetAddress myRemoteLocation; 

float d1 = 20;
float d2 = 300;
float d3 = d2-30;

float send;
boolean customVowel = false;

PImage m0;
PImage m1;


void setup() {
  //DATASENDER
  size(600, 190);
  smooth();
  frameRate(25);
  oscP5 = new OscP5(this, 9000);
  myRemoteLocation = new NetAddress("127.0.0.1", 9010);

  noStroke();
  cp5 = new ControlP5(this);

  m0 = loadImage("m0.png");
  m1 = loadImage("m1.png");

  //MORE SLIDERS

  cp5.addSlider("a1")
    .setPosition (d1, 20)
    .setSize (50, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;

  cp5.addSlider("a1T")
    .setPosition (d1, 150)
    .setSize (50, 20)
    .setRange(0, 2000)
    ;

  cp5.addSlider("a2")
    .setPosition (d1+50, 20)
    .setSize (50, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;

  cp5.addSlider("a2T")
    .setPosition (d1+50, 150)
    .setSize (50, 20)
    .setRange(0, 2000)
    ;

  cp5.addSlider("a3")
    .setPosition (d1+100, 20)
    .setSize (50, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;

  cp5.addSlider("a3T")
    .setPosition (d1+100, 150)
    .setSize (50, 20)
    .setRange(0, 2000)
    ;

  cp5.addSlider("a4")
    .setPosition (d1+150, 20)
    .setSize (50, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;

  cp5.addSlider("a4T")
    .setPosition (d1+150, 150)
    .setSize (50, 20)
    .setRange(0, 2000)
    ;

  cp5.addSlider("h0")
    .setPosition (d3+35*0, 20)
    .setSize (10, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;
  cp5.addSlider("h1")
    .setPosition (d3+35*1, 20)
    .setSize (10, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;
  cp5.addSlider("h2")
    .setPosition (d3+35*2, 20)
    .setSize (10, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;
  cp5.addSlider("h3")
    .setPosition (d3+35*3, 20)
    .setSize (10, 100)
    .setRange(0, 1)
    .setColorBackground( color(255, 0, 0))
    .setColorForeground(color(0, 0, 255))
    .setColorActive(color(0, 255, 0))
    ;

  cp5.addSlider("f1")
    .setPosition (d3, 145)
    .setSize (122, 10)
    .setRange(100, 1000)
    ;
  cp5.addSlider("f2")
    .setPosition (d3, 160)
    .setSize (122, 10)
    .setRange(500, 2500)
    ;

  cp5.addButton("A")
    .setPosition(d3+150, 20)
    .setSize(25, 25)
    .setValue(0)
    ;
  cp5.addButton("E")
    .setPosition(d3+150, 20+25)
    .setSize(25, 25)
    .setValue(0)
    ;
  cp5.addButton("I")
    .setPosition(d3+150, 20+25*2)
    .setSize(25, 25)
    .setValue(0)
    ;
  cp5.addButton("O")
    .setPosition(d3+150, 20+25*3)
    .setSize(25, 25)
    .setValue(0)
    ;
  cp5.addButton("U")
    .setPosition(d3+150, 20+25*4)
    .setSize(25, 25)
    .setValue(0)
    ;
}

void draw() {
  background(0);

  //friendly reminder
  textSize(12);
  text("press s for sending data over to PD", 10, 15);
  image(m0, 550, 130);
} 

void keyPressed() {
  //la idea seria meter esto en un bang but welp
  //quiza poner los bangs en el sitio pero siendo inutiles y ponerle un if (esta en estas coordenadas (las del bang) && clickas pos te pasa
  //y entonces el bang es solo adorno but it cute there isnt it

  if (key == '1') {
    cp5.getController("a1").setValue(0.1);
    cp5.getController("a1T").setValue(100);

    cp5.getController("a2").setValue(0.1);
    cp5.getController("a2T").setValue(100);

    cp5.getController("a3").setValue(0.1);
    cp5.getController("a3T").setValue(100);

    cp5.getController("a4").setValue(0.1);
    cp5.getController("a4T").setValue(100);
  }
  if (key == '2') {
    cp5.getController("a1").setValue(0);
    cp5.getController("a1T").setValue(100);

    cp5.getController("a2").setValue(0);
    cp5.getController("a2T").setValue(0);

    cp5.getController("a3").setValue(0);
    cp5.getController("a3T").setValue(0);

    cp5.getController("a4").setValue(0);
    cp5.getController("a4T").setValue(0);
  }
  if (key == '3') {
    cp5.getController("a1").setValue(1);
    cp5.getController("a1T").setValue(100);

    cp5.getController("a2").setValue(0.5);
    cp5.getController("a2T").setValue(100);

    cp5.getController("a3").setValue(0.5);
    cp5.getController("a3T").setValue(150);
  }
  if (key == '4') {
    cp5.getController("a1").setValue(0.5);
    cp5.getController("a1T").setValue(100);

    cp5.getController("a2").setValue(0);
    cp5.getController("a2T").setValue(100);

    cp5.getController("a3").setValue(0);
    cp5.getController("a3T").setValue(0);

    cp5.getController("a4").setValue(0);
    cp5.getController("a4T").setValue(0);
  }
  if (key == '5') {
    cp5.getController("a1").setValue(0.5);
    cp5.getController("a1T").setValue(100);

    cp5.getController("a2").setValue(0.5);
    cp5.getController("a2T").setValue(100);

    cp5.getController("a3").setValue(0);
    cp5.getController("a3T").setValue(100);

    cp5.getController("a4").setValue(0);
    cp5.getController("a4T").setValue(0);
  }
  if (key == '0') {
    cp5.getController("a1").setValue(0);
    cp5.getController("a1T").setValue(0);

    cp5.getController("a2").setValue(0);
    cp5.getController("a2T").setValue(0);

    cp5.getController("a3").setValue(0);
    cp5.getController("a3T").setValue(0);

    cp5.getController("a4").setValue(0);
    cp5.getController("a4T").setValue(0);
  }

  if (key == 'q') {
    cp5.getController("h0").setValue(1);
    cp5.getController("h1").setValue(0);    
    cp5.getController("h2").setValue(0);
    cp5.getController("h3").setValue(0);
  }
  if (key == 'w') {
    cp5.getController("h0").setValue(1);
    cp5.getController("h1").setValue(1);    
    cp5.getController("h2").setValue(1);
    cp5.getController("h3").setValue(1);
  }
  if (key == 'e') {
    cp5.getController("h0").setValue(0);
    cp5.getController("h1").setValue(1);    
    cp5.getController("h2").setValue(0);
    cp5.getController("h3").setValue(1);
  }
  if (key == 'r') {
    cp5.getController("h0").setValue(0);
    cp5.getController("h1").setValue(0);    
    cp5.getController("h2").setValue(1);
    cp5.getController("h3").setValue(0);
  }
  if (key == 't') {
    cp5.getController("h0").setValue(1);
    cp5.getController("h1").setValue(0);    
    cp5.getController("h2").setValue(0);
    cp5.getController("h3").setValue(1);
  }
  if (key == 'y') {
    cp5.getController("h0").setValue(0);
    cp5.getController("h1").setValue(0);    
    cp5.getController("h2").setValue(0);
    cp5.getController("h3").setValue(0);
  }
  if (key == 'p') {
    println(cp5.getValue("customVowel"));
  }


  if (key == 's') {
    //ESTO ES DEL DATASENDER
    int totalDelay1= int(cp5.getValue("a2T")) + int(cp5.getValue("a3T"));
    int totalDelay2= totalDelay1 + int(cp5.getValue("a4T"));

    OscMessage myMessage = new OscMessage("/a");
    myMessage.add(cp5.getValue("a1")); /* add an int to the osc message */
    myMessage.add(cp5.getValue("a1T")); /* add an int to the osc message */
    //myMessage.add(cp5.getValue(","));

    myMessage.add(cp5.getValue("a2")); /* add an int to the osc message */
    myMessage.add(cp5.getValue("a2T")); /* add an int to the osc message */
    myMessage.add(cp5.getValue("a2T"));
    //myMessage.add(cp5.getValue(","));

    myMessage.add(cp5.getValue("a3")); /* add an int to the osc message */
    myMessage.add(cp5.getValue("a3T")); /* add an int to the osc message */
    myMessage.add(totalDelay1);
    //myMessage.add(cp5.getValue(","));

    myMessage.add(cp5.getValue("a4")); /* add an int to the osc message */
    myMessage.add(cp5.getValue("a4T")); /* add an int to the osc message */
    myMessage.add(totalDelay2);

    myMessage.add(cp5.getValue("h0")); //AQUI AQUI AQUI

    myMessage.add(cp5.getValue("h0"));
    myMessage.add(cp5.getValue("h1"));
    myMessage.add(cp5.getValue("h2"));
    myMessage.add(cp5.getValue("h3"));

    myMessage.add(cp5.getValue("f1"));
    myMessage.add(cp5.getValue("f2"));

    image(m1, 550, 130);
    //molaria que pudiese mantenerse la imagen m1 el tiempo del totalDelay1
    /* send the message */
    oscP5.send(myMessage, myRemoteLocation);
  }
}

public void A() {
  cp5.getController("f1").setValue(850);
  cp5.getController("f2").setValue(1610);
}
public void E() {
  cp5.getController("f1").setValue(610);
  cp5.getController("f2").setValue(1900);
}
public void I() {
  cp5.getController("f1").setValue(240);
  cp5.getController("f2").setValue(2400);
}
public void O() {
  cp5.getController("f1").setValue(360);
  cp5.getController("f2").setValue(640);
}
public void U() {
  cp5.getController("f1").setValue(250);
  cp5.getController("f2").setValue(595);
}
